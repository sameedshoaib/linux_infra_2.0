#!/bin/bash

#installing OVPN 

mv /etc/openvpn/server-udp.conf /root/server-udp.conf
mv /etc/openvpn/server-tcp.conf /root/server-tcp.conf

mv /etc/openvpn/radiusplugin-tcp.cnf /root/radiusplugin-tcp.cnf
mv /etc/openvpn/radiusplugin-udp.cnf /root/radiusplugin-udp.cnf 

wget https://swupdate.openvpn.org/community/releases/openvpn-2.4.6.tar.gz
tar xzf openvpn-2.4.6.tar.gz
cd openvpn-2.4.6
./configure
make
make install


rm -rf /etc/openvpn
cp -r /root/purevpn-linuxbox-v2/vpn-engines/openvpn/ /etc/

rm -rf /etc/openvpn/server-udp.conf
rm -rf /etc/openvpn/server-tcp.conf
rm -rf /etc/openvpn/radiusplugin-tcp.cnf
rm -rf /etc/openvpn/radiusplugin-udp.cnf

mv /root/server-udp.conf /etc/openvpn/server-udp.conf 
mv /root/server-tcp.conf /etc/openvpn/server-tcp.conf
mv  /root/radiusplugin-tcp.cnf /etc/openvpn/radiusplugin-tcp.cnf
mv  /root/radiusplugin-udp.cnf /etc/openvpn/radiusplugin-udp.cnf

sed -i -e '/cipher AES-256-CBC/d' /etc/openvpn/server-udp.conf 
sed -i -e '/cipher AES-256-CBC/d' /etc/openvpn/server-tcp.conf

sed -i '$ a\tun-mtu 48000' /etc/openvpn/server-udp.conf 
sed -i '$ a\fragment 0' /etc/openvpn/server-udp.conf 
sed -i '$ a\mssfix 0' /etc/openvpn/server-udp.conf 
sed -i '$ a\cipher AES-256-GCM' /etc/openvpn/server-udp.conf 
sed -i '$ a\tls-version-min 1.2' /etc/openvpn/server-udp.conf 
sed -i '$ a\tls-cipher TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384:TLS-ECDHE-ECDSA-WITH-AES-256-GCM-SHA384:TLS-ECDHE-RSA-WITH-AES-256-CBC-SHA384:TLS-ECDHE-ECDSA-WITH-AES-256-CBC-SHA384:TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256' /etc/openvpn/server-udp.conf 

sed -i '$ a\tun-mtu 48000' /etc/openvpn/server-tcp.conf
sed -i '$ a\fragment 0' /etc/openvpn/server-tcp.conf
sed -i '$ a\mssfix 0' /etc/openvpn/server-tcp.conf
sed -i '$ a\cipher AES-256-GCM' /etc/openvpn/server-tcp.conf
sed -i '$ a\tls-version-min 1.2' /etc/openvpn/server-tcp.conf
sed -i '$ a\tls-cipher TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384:TLS-ECDHE-ECDSA-WITH-AES-256-GCM-SHA384:TLS-ECDHE-RSA-WITH-AES-256-CBC-SHA384:TLS-ECDHE-ECDSA-WITH-AES-256-CBC-SHA384:TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256' /etc/openvpn/server-tcp.conf




#!/bin/bash
cd
wget https://www.openssl.org/source/openssl-1.0.2o.tar.gz
yes | mv /usr/local/bin/openssl /usr/local/bin/openssl-old
tar xzf openssl-1.0.2o.tar.gz
cd openssl-1.0.2o
./config shared -Wl,-rpath=/usr/local/ssl/lib --prefix=/usr/local/ssl
sudo make
sudo make test
sudo make install
sudo ln -s /usr/local/ssl/bin/openssl /usr/local/bin/openssl
cd
pkill -9 openvpn
systemctl restart openvpn-tcp@server
systemctl restart openvpn-udp@server



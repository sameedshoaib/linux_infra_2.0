#!/bin/bash

#------------------
# Cron conf 
#------------------
cat > crontab.txt <<EOF
# At Boot time Set Radius Priority
@reboot /root/purevpn-linuxbox-v2/crons/radius-priority.sh

# After Every 1 mint
* * * * * /root/purevpn-linuxbox-v2/crons/ServiceCheck.sh

# After Every 30 mints
0,30 * * * * /root/purevpn-linuxbox-v2/crons/UpdateAccounting.sh
EOF

crontab crontab.txt
rm -rf crontab.txt


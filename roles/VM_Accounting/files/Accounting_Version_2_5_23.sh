#!/bin/bash

cd /root/applications/
wget https://www.dropbox.com/s/p8ticn700c3xccb/linux-accountings.tar.gz
systemctl stop accounting
yes | mv -f linux-accountings linux-accountings-2.5.22.old-20.10.2018
tar -xf linux-accountings.tar.gz
rm -rf linux-accountings.tar.gz
yes | cp -r linux-accountings-2.5.22.old-20.10.2018/GeoIP.dat /root/applications/linux-accountings
yes | cp -r linux-accountings-2.5.22.old-20.10.2018/AccountingScript.sh /root/applications/linux-accountings
yes | cp -r linux-accountings-2.5.22.old-20.10.2018/AccountingServices.yml /root/applications/linux-accountings
yes | cp -r linux-accountings-2.5.22.old-20.10.2018/accounting.service /root/applications/linux-accountings
sed -i 's/api.platform.purevpn.com/api.atom.purevpn.com/g' /root/applications/linux-accountings/AccountingServices.yml
statustcp="$(cat /root/applications/linux-accountings/AccountingServices.yml | grep -o "STATUS_TCP:")"
if [ "$statustcp" = "STATUS_TCP:" ]
 then
    echo "nothing to do"
 else
    var="$(grep -n "    OPENVPN:" linux-accountings/AccountingServices.yml | awk -F ":" {'print $1'})"
    var2=$((var + 1))
    sed -i "${var2}a\        # STATUS_TCP: 'disabled'" /root/applications/linux-accountings/AccountingServices.yml
    var3=$((var + 2))
    sed -i "${var3}a\        # STATUS_UDP: 'disabled'" /root/applications/linux-accountings/AccountingServices.yml
fi
rm -rf /root/applications/linux-accountings-2.5.22.old-20.10.2018/logs
systemctl start accounting
sleep 2
updatedb
systemctl restart accounting
systemctl status accounting




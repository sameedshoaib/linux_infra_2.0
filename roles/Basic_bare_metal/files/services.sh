#!/bin/bash

#------------------
# Kasper conf 
#------------------
rpm -ivh /root/purevpn-linuxbox-v2/services/kaspersky/klnagent-10.1.0-61.i386.rpm
cat > answer.txt << EOF
ksc-main.smarthox.net
14000
13000
Y
1
EOF

/opt/kaspersky/klnagent/lib/bin/setup/postinstall.pl < answer.txt
rm -rf answer.txt
/opt/kaspersky/klnagent/bin/klnagchk

#------------------
# SIEM conf 
#------------------
rpm -ivh /root/purevpn-linuxbox-v2/services/mcafee-siem/mcafee-siem-collector-11.00.4150-1575.i686.rpm
rm -rf /opt/McAfee && cp -r /root/purevpn-linuxbox-v2/services/mcafee-siem/McAfee /opt/
service mcafee_siem_collector start
service mcafee_siem_collector stop
service mcafee_siem_collector start

#------------------
# Accounting conf 
#------------------
wget https://www.dropbox.com/s/hrex96j0osbmaui/linux-accountings-2.5.14.tar.gz?dl=1
systemctl stop accounting

mv linux-accountings-2.5.14.tar.gz?dl=1 linux-accountings.tar.gz
tar -xf "linux-accountings.tar.gz"

systemctl stop accounting
rm -rf "linux-accountings.tar.gz"
rm -rf /root/applications/
rm -rf /etc/systemd/system/accounting.service

mkdir -p /root/applications/
mv linux-accountings /root/applications/

cd /root/purevpn-linuxbox-v2/services/accounting
cp AccountingServices.yml accounting.service AccountingScript.sh GeoIP.dat /root/applications/linux-accountings/
cp  accounting.service /etc/systemd/system/


cd ~
systemctl daemon-reload
systemctl disable accounting
#------------------
# Cron conf 
#------------------
cat > crontab.txt <<EOF
# At Boot time Set Radius Priority
@reboot /root/purevpn-linuxbox-v2/crons/radius-priority.sh

# After Every 1 mint
* * * * * /root/purevpn-linuxbox-v2/crons/ServiceCheck.sh

# After Every 30 mints
0,30 * * * * /root/purevpn-linuxbox-v2/crons/UpdateAccounting.sh
EOF

crontab crontab.txt
rm -rf crontab.txt

#-------------------------------------
# User creation
#-------------------------------------
useradd rootadmintek
echo abc123+ |  passwd --stdin rootadmintek


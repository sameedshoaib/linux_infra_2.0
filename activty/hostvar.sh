#!/bin/bash
curl -k -s 'https://devops.dns2use.com/apiserver/lux-40-04-04/?format=json' > Json_sample.txt
server_name=($(jq -r '.name' Json_sample.txt))
server_name=${server_name//[[:blank:]]/}

echo "---"  > $server_name

echo "#--------------------------------------" >> $server_name
echo "# Part of infra">> $server_name
echo "#--------------------------------------" >> $server_name
temp=($(jq -r '.sInfra' Json_sample.txt))
temp=${temp//[[:blank:]]/}
echo "infra: ${temp[1]}" >> $server_name

echo "#--------------------------------------" >> $server_name
echo "# KVM  details (Type none if bare_metal)" >> $server_name
echo "#--------------------------------------" >> $server_name
temp=($(jq -r '.kvm_ip' Json_sample.txt))
temp=${temp//[[:blank:]]/}
machine_name=$(echo $server_name | cut -f2 -dx)
kvm_name="kvm$machine_name"
kvm_name=${kvm_name//[[:blank:]]/}

 if [[ $temp == "None" ]]
    then
    temp="none"
    echo "KvmHostname: $temp" >> $server_name
    echo "KvmIP: $temp" >> $server_name

    else
    echo "KvmHostname: $kvm_name" >> $server_name
    echo "KvmIP: $temp" >> $server_name
    fi

echo "#--------------------------" >> $server_name
echo "# VM or bare_metal details " >> $server_name
echo "#-------------------------" >> $server_name

echo "LuxHostname: $server_name" >> $server_name

temp=($(jq -r '.server_ip' Json_sample.txt))
temp=${temp//[[:blank:]]/}
echo "LuxIP: $temp" >> $server_name

temp=($(jq -r '.server_subnet' Json_sample.txt))
temp=${temp//[[:blank:]]/}
PriNetwork=$(echo $temp | cut -f1 -d/)
PriPrefix=$(echo $temp | cut -f2 -d/)
echo "PriNetwork: $PriNetwork" >> $server_name
echo "PriPrefix: $PriPrefix" >> $server_name


temp=($(jq -r '.server_gateway' Json_sample.txt))
temp=${temp//[[:blank:]]/}
echo "PriGateway: $temp" >> $server_name


temp=($(jq -r '.server_added_pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}
SecNetwork=$(echo $temp | cut -f1 -d/)
SecPrefix=$(echo $temp | cut -f2 -d/)
echo "SecNetwork: $SecNetwork" >> $server_name
echo "SecPrefix: $SecPrefix" >> $server_name


temp=($(jq -r '.dns_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}
DNSIP=$(echo $temp | cut -f1 -d,)
DNSIP1=$(echo $temp | cut -f2 -d,)
echo "DNSIP: $DNSIP" >> $server_name
echo "DNSIP1: $DNSIP1" >> $server_name

temp=($(jq -r '.swan_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}
echo "SecIP1: $temp" >> $server_name

temp=($(jq -r '.openvpn_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}
echo "SecIP2: $temp" >> $server_name

temp=($(jq -r '.strongSwan_pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}

SS_start_IP=$(echo $temp | cut -f1 -d-)
SS_end_IP=$(echo $temp | cut -f2 -d-)
echo "SS_start_IP: $SS_start_IP" >> $server_name

first_octate=$(echo $SS_start_IP | cut -f1 -d.)
second_octate=$(echo $SS_start_IP | cut -f2 -d.)
third_octate=$(echo $SS_start_IP | cut -f3 -d.)
SS_end_IP=$(echo $temp | cut -f2 -d-)
SS_end_IP="$first_octate.$second_octate.$third_octate.$SS_end_IP"

echo "SS_end_IP: $SS_end_IP" >> $server_name

temp=($(jq -r '.ovpnTCP_pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}

OVPN_TCP_IP=$(echo $temp | cut -f1 -d/)
OVPN_TCP_SUBNET_MASQ=$(echo $temp | cut -f2 -d/)
OVPN_TCP_SUBNET_MASQ=${OVPN_TCP_SUBNET_MASQ//[[:blank:]]/}
echo "OVPN_TCP_IP: $OVPN_TCP_IP" >> $server_name


 if [[ "$OVPN_TCP_SUBNET_MASQ" == "21" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.248.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "22" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.252.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "23" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.254.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "24" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "25" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.128"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "26" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.192"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "27" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.224"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "28" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.240"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "29" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.248"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "30" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.252"
    fi

echo "OVPN_TCP_SUBNET_MASQ: $OVPN_TCP_SUBNET_MASQ" >> $server_name

temp=($(jq -r '.ovpnUDP_Pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}

OVPN_UDP_IP=$(echo $temp | cut -f1 -d/)
OVPN_UDP_SUBNET_MASQ=$(echo $temp | cut -f2 -d/)
OVPN_UDP_SUBNET_MASQ=${OVPN_UDP_SUBNET_MASQ//[[:blank:]]/}
echo "OVPN_UDP_IP: $OVPN_UDP_IP" >> $server_name

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "21" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.248.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "22" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.252.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "23" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.254.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "24" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "25" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.128"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "26" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.192"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "27" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.224"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "28" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.240"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "29" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.248"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "30" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.252"
    fi

echo "OVPN_UDP_SUBNET_MASQ: $OVPN_UDP_SUBNET_MASQ" >> $server_name



echo "#------------------" >> $server_name
echo "# Data for Services" >> $server_name
echo "#------------------" >> $server_name
temp=($(jq -r '.server_countryID' Json_sample.txt))
ISO_Code=$(echo ${temp[@]} | cut -f2 -d-)
ISO_Code=${ISO_Code//[[:blank:]]/}
echo "ISO_Code: $ISO_Code" >> $server_name

echo "SensuPass: enter_sensu_pass" >> $server_name

echo "nat_server: false" >> $server_name

temp=($(jq -r '.p2p' Json_sample.txt))
temp=${temp//[[:blank:]]/}

 if [[ $temp == "allow" || $temp == "allowed" || $temp == "Allow" || $temp == "Allowed" ]]
    then
    echo "Torrenting: true" >> $server_name
    else
    echo "Torrenting: false" >> $server_name
    fi
echo "#-----------------" >> $server_name
echo "# Disable Services " >> $server_name
echo "#------------------" >> $server_name

temp=($(jq -r '.swan_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}

 if [[ $temp == "None" || $temp == "none" ]]
    then
    echo "IKE_disable: true" >> $server_name
    else
    echo "IKE_disable: false" >> $server_name
    fi

temp=($(jq -r '.openvpn_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}

 if [[ $temp == "None" || $temp == "none" ]]
    then
    echo "OVPN_disable: true" >> $server_name
    else
    echo "OVPN_disable: false" >> $server_name
    fi

 if [[ $OVPN_UDP_IP == "None" || $OVPN_UDP_IP == "none" ]]
    then
    echo "OVPN_UDP_disable: true" >> $server_name
    else
    echo "OVPN_UDP_disable: false" >> $server_name
    fi

 if [[ $OVPN_TCP_IP == "None" || $OVPN_UDP_IP == "none" ]]
    then
    echo "OVPN_TCP_disable: true" >> $server_name
    else
    echo "OVPN_TCP_disable: false" >> $server_name
    fi
echo "#--------------------------------------------------" >> $server_name
echo "# 1.75 configuration (exclude if not a part of 1.75) " >> $server_name
echo "#---------------------------------------------------" >> $server_name
echo "Private_IP_three_octate_vm: 172.16.2" >> $server_name
echo "#---------------------------------------------------" >> $server_name
echo "# vFW Configuration (exclude if not a part of 1.75) " >> $server_name
echo "#-----------------------------------------------------" >> $server_name
vfw_name="vfw$machine_name"
echo "FwHostname: $vfw_name" >> $server_name
echo "FwIP: none" >> $server_name







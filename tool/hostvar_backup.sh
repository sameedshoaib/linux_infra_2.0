#!/bin/bash
#echo "Enter the server name like (lux-01-01-01) :" 
#read varname
read -p 'Enter the server name like (lux-01-01-01) : ' varname
varname=${varname//[[:blank:]]/}

curl -k -s "https://devops.dns2use.com/apiserver/$varname/?format=json" > Json_sample.txt
server_name=($(jq -r '.name' Json_sample.txt))
server_name=${server_name//[[:blank:]]/}

echo "---"  > $server_name

echo "#--------------------------------------" >> $server_name
echo "# Part of infra">> $server_name
echo "#--------------------------------------" >> $server_name
infra=($(jq -r '.sInfra' Json_sample.txt))
infra=${infra//[[:blank:]]/}
echo "infra: ${infra[1]}" >> $server_name

echo "#--------------------------------------" >> $server_name
echo "# KVM  details (Type none if bare_metal)" >> $server_name
echo "#--------------------------------------" >> $server_name
KvmIP=($(jq -r '.kvm_ip' Json_sample.txt))
KvmIP=${KvmIP//[[:blank:]]/}
machine_name=$(echo $server_name | cut -f2 -dx)
kvm_name="kvm$machine_name"
kvm_name=${kvm_name//[[:blank:]]/}

 if [[ $KvmIP == "None" || $KvmIP == "none" ]]
    then
    temp="none"
    echo "KvmHostname: $temp" >> $server_name
    echo "KvmIP: $temp" >> $server_name

    else
    echo "KvmHostname: $kvm_name" >> $server_name
    echo "KvmIP: $KvmIP" >> $server_name
    fi

echo "#--------------------------" >> $server_name
echo "# VM or bare_metal details " >> $server_name
echo "#-------------------------" >> $server_name

echo "LuxHostname: $server_name" >> $server_name

LuxIP=($(jq -r '.server_ip' Json_sample.txt))
LuxIP=${LuxIP//[[:blank:]]/}
echo "LuxIP: $LuxIP" >> $server_name

temp=($(jq -r '.server_subnet' Json_sample.txt))
temp=${temp//[[:blank:]]/}
PriNetwork=$(echo $temp | cut -f1 -d/)
PriPrefix=$(echo $temp | cut -f2 -d/)
echo "PriNetwork: $PriNetwork" >> $server_name
echo "PriPrefix: $PriPrefix" >> $server_name


PriGateway=($(jq -r '.server_gateway' Json_sample.txt))
PriGateway=${PriGateway//[[:blank:]]/}
echo "PriGateway: $PriGateway" >> $server_name


temp=($(jq -r '.server_added_pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}

SecNetwork1=$(echo $temp | cut -f1 -d,)
Public_SecNetwork1=$(echo $temp | cut -f2 -d,)

SecNetwork1=${SecNetwork1//[[:blank:]]/}
Public_SecNetwork1=${Public_SecNetwork1//[[:blank:]]/}

Public_SecNetwork=$(echo $Public_SecNetwork1 | cut -f1 -d/)
Public_SecPrefix=$(echo $Public_SecNetwork1 | cut -f2 -d/)

SecNetwork=$(echo $SecNetwork1 | cut -f1 -d/)
SecPrefix=$(echo $SecNetwork1 | cut -f2 -d/)


echo "SecNetwork: $SecNetwork" >> $server_name
echo "SecPrefix: $SecPrefix" >> $server_name


temp=($(jq -r '.dns_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}
DNSIP=$(echo $temp | cut -f1 -d,)
DNSIP1=$(echo $temp | cut -f2 -d,)
echo "DNSIP: $DNSIP" >> $server_name
echo "DNSIP1: $DNSIP1" >> $server_name

temp=($(jq -r '.swan_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}
echo "SecIP1: $temp" >> $server_name

temp=($(jq -r '.openvpn_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}
echo "SecIP2: $temp" >> $server_name

temp=($(jq -r '.strongSwan_pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}

#SS_start_IP=$(echo $temp | cut -f1 -d-)
#SS_end_IP=$(echo $temp | cut -f2 -d-)
#echo "SS_start_IP: $SS_start_IP" >> $server_name
#first_octate=$(echo $SS_start_IP | cut -f1 -d.)
#second_octate=$(echo $SS_start_IP | cut -f2 -d.)
#third_octate=$(echo $SS_start_IP | cut -f3 -d.)
#SS_end_IP=$(echo $temp | cut -f2 -d-)
#SS_end_IP="$first_octate.$second_octate.$third_octate.$SS_end_IP"
#echo "SS_end_IP: $SS_end_IP" >> $server_name

SS_start_IP=$(echo $temp | cut -f1 -d-)
SS_end_IP=$(echo $temp | cut -f2 -d-)
echo "SS_start_IP: $SS_start_IP" >> $server_name
echo "SS_end_IP: $SS_end_IP" >> $server_name


temp=($(jq -r '.ovpnTCP_pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}

OVPN_TCP_IP=$(echo $temp | cut -f1 -d/)
OVPN_TCP_SUBNET_MASQ=$(echo $temp | cut -f2 -d/)
OVPN_TCP_SUBNET_MASQ=${OVPN_TCP_SUBNET_MASQ//[[:blank:]]/}
echo "OVPN_TCP_IP: $OVPN_TCP_IP" >> $server_name


 if [[ "$OVPN_TCP_SUBNET_MASQ" == "21" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.248.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "22" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.252.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "23" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.254.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "24" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.0"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "25" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.128"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "26" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.192"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "27" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.224"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "28" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.240"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "29" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.248"
    fi

 if [[ "$OVPN_TCP_SUBNET_MASQ" == "30" ]]
    then
    OVPN_TCP_SUBNET_MASQ="255.255.255.252"
    fi

echo "OVPN_TCP_SUBNET_MASQ: $OVPN_TCP_SUBNET_MASQ" >> $server_name

temp=($(jq -r '.ovpnUDP_Pool' Json_sample.txt))
temp=${temp//[[:blank:]]/}

OVPN_UDP_IP=$(echo $temp | cut -f1 -d/)
OVPN_UDP_SUBNET_MASQ=$(echo $temp | cut -f2 -d/)
OVPN_UDP_SUBNET_MASQ=${OVPN_UDP_SUBNET_MASQ//[[:blank:]]/}
echo "OVPN_UDP_IP: $OVPN_UDP_IP" >> $server_name

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "21" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.248.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "22" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.252.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "23" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.254.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "24" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.0"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "25" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.128"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "26" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.192"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "27" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.224"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "28" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.240"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "29" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.248"
    fi

 if [[ "$OVPN_UDP_SUBNET_MASQ" == "30" ]]
    then
    OVPN_UDP_SUBNET_MASQ="255.255.255.252"
    fi

echo "OVPN_UDP_SUBNET_MASQ: $OVPN_UDP_SUBNET_MASQ" >> $server_name



echo "#------------------" >> $server_name
echo "# Data for Services" >> $server_name
echo "#------------------" >> $server_name
temp=($(jq -r '.server_countryID' Json_sample.txt))
ISO_Code=$(echo ${temp[@]} | cut -f2 -d-)
ISO_Code=${ISO_Code//[[:blank:]]/}
echo "ISO_Code: $ISO_Code" >> $server_name

temp=($(jq -r '.p2p' Json_sample.txt))
temp=${temp//[[:blank:]]/}

 if [[ $temp == "allow" || $temp == "allowed" || $temp == "Allow" || $temp == "Allowed" ]]
    then
    echo "Torrenting: allow" >> $server_name
    else
    echo "Torrenting: Blocked" >> $server_name
    fi

first_octate=$(echo $SecNetwork| cut -f1 -d.)
second_octate=$(echo $SecNetwork | cut -f2 -d.)

first_octate=${first_octate//[[:blank:]]/}
second_octate=${second_octate//[[:blank:]]/}

second_octate="$first_octate.$second_octate"
second_octate=${second_octate//[[:blank:]]/}

#class_A
 if [[ "$first_octate" == "10" ]]
    then
    echo "nat_server: true" >> $server_name

#class_B
    elif [[ "$second_octate" == "172.16" || "$second_octate" == "172.17" || "$second_octate" == "172.18" || "$second_octate" == "172.19" || "$second_octate" == "172.20" || "$second_octate" == "172.21" || "$second_octate" == "172.22" || "$second_octate" == "172.23" || "$second_octate" == "172.24" || "$second_octate" == "172.25" || "$second_octate" == "172.26" || "$second_octate" == "172.27" || "$second_octate" == "172.28" || "$second_octate" == "172.29" || "$second_octate" == "172.30" || "$second_octate" == "172.31" ]]
    then
    echo "nat_server: true" >> $server_name

#class_c
    elif [[ "$second_octate" == "192.168" ]]
    then
    echo "nat_server: true" >> $server_name

    else
    echo "nat_server: false" >> $server_name
    fi

sensu_pass=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)
echo "SensuPass: $sensu_pass" >> $server_name

echo "#-----------------" >> $server_name
echo "# Disable Services " >> $server_name
echo "#------------------" >> $server_name

temp=($(jq -r '.swan_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}

 if [[ $temp == "None" || $temp == "none" ]]
    then
    echo "IKE_disable: true" >> $server_name
    else
    echo "IKE_disable: false" >> $server_name
    fi

temp=($(jq -r '.openvpn_listenIP' Json_sample.txt))
temp=${temp//[[:blank:]]/}

 if [[ $temp == "None" || $temp == "none" ]]
    then
    echo "OVPN_disable: true" >> $server_name
    else
    echo "OVPN_disable: false" >> $server_name
    fi

 if [[ $OVPN_UDP_IP == "None" || $OVPN_UDP_IP == "none" ]]
    then
    echo "OVPN_UDP_disable: true" >> $server_name
    else
    echo "OVPN_UDP_disable: false" >> $server_name
    fi

 if [[ $OVPN_TCP_IP == "None" || $OVPN_TCP_IP == "none" ]]
    then
    echo "OVPN_TCP_disable: true" >> $server_name
    else
    echo "OVPN_TCP_disable: false" >> $server_name
    fi
echo "#--------------------------------------------------" >> $server_name
echo "# 1.75 configuration (exclude if not a part of 1.75) " >> $server_name
echo "#---------------------------------------------------" >> $server_name
Private_IP_three_octate_vm=$(echo $server_name | cut -f4 -d-)
Private_IP_three_octate_vm=${Private_IP_three_octate_vm//[[:blank:]]/}
Private_IP_three_octate_vm=$(echo $Private_IP_three_octate_vm | sed 's/^0*//')
 
 if [[ ${infra[1]} == "1.75" ]]
    then
    echo "Private_IP_three_octate_vm: 172.16.$Private_IP_three_octate_vm" >> $server_name
    
    else
    echo "Private_IP_three_octate_vm: none" >> $server_name
    fi



echo "#---------------------------------------------------" >> $server_name
echo "# vFW Configuration (exclude if not a part of 1.75) " >> $server_name
echo "#-----------------------------------------------------" >> $server_name

kvm_first_octate=$(echo $KvmIP | cut -f1 -d.)
kvm_second_octate=$(echo $KvmIP | cut -f2 -d.)
kvm_third_octate=$(echo $KvmIP | cut -f3 -d.)
kvm_forth_octate=$(echo $KvmIP | cut -f4 -d.)
FwIP_octate=$(( kvm_forth_octate + 2 ))
FwIP="$kvm_first_octate.$kvm_second_octate.$kvm_third_octate.$FwIP_octate"

 if [[ ${infra[1]} == "1.75" ]]
    then
    vfw_name="vfw$machine_name"
    echo "FwHostname: $vfw_name" >> $server_name
    echo "FwIP: $FwIP" >> $server_name
    
    else
    echo "FwHostname: none" >> $server_name
    echo "FwIP: none" >> $server_name
    fi

echo "##--------------------------" >> $server_name
echo "## Killping VM details " >> $server_name
echo "##-------------------------" >> $server_name

if [[ $KvmIP == "None" || $KvmIP == "none" ]]
   then
echo "KHostname: none " >> $server_name
echo "KIP: none" >> $server_name
echo "netmask: none" >> $server_name
echo "network: none" >> $server_name
echo "broadcast: none" >> $server_name
echo "gateway: none" >> $server_name

else

machine_name=$(echo $server_name | cut -f2 -d-)
machine_name1=$(echo $server_name | cut -f3 -d-)
machine_name2=$(echo $server_name | cut -f4 -d-)
kp_name="kp-$machine_name-$machine_name1-$machine_name2"
kp_name=${kp_name//[[:blank:]]/}
echo "KHostname: $kp_name" >> $server_name

KIP_octate=$(( kvm_forth_octate + 3 ))
KIP="$kvm_first_octate.$kvm_second_octate.$kvm_third_octate.$KIP_octate"
echo "KIP: $KIP" >> $server_name

 if [[ "$PriPrefix" == "8"  ]]
    then
    netmask="255.0.0.0"
    Public_SecPrefix_netmask="255.0.0.0"
    fi
   
 if [[ "$PriPrefix" == "9"  ]]
    then
    netmask="255.128.0.0"
    Public_SecPrefix_netmask="255.128.0.0"
    fi
	
 if [[ "$PriPrefix" == "10"  ]]
    then
    netmask="255.192.0.0"
    Public_SecPrefix_netmask="255.192.0.0"
    fi
	
 if [[ "$PriPrefix" == "11"  ]]
    then
    netmask="255.224.0.0"
    Public_SecPrefix_netmask="255.224.0.0"
    fi
   
 if [[ "$PriPrefix" == "12"  ]]
    then
    netmask="255.240.0.0"
    Public_SecPrefix_netmask="255.240.0.0"
    fi
	
 if [[ "$PriPrefix" == "13"  ]]
    then
    netmask="255.248.0.0"
    Public_SecPrefix_netmask="255.248.0.0"
    fi
	
 if [[ "$PriPrefix" == "14"  ]]
    then
    netmask="255.252.0.0"
    Public_SecPrefix_netmask="255.252.0.0"
    fi
   
 if [[ "$PriPrefix" == "15"  ]]
    then
    netmask="255.252.0.0"
    Public_SecPrefix_netmask="255.252.0.0"
    fi
  
 if [[ "$PriPrefix" == "16"  ]]
    then
    netmask="255.255.0.0"
    Public_SecPrefix_netmask="255.255.0.0"
    fi
 
 if [[ "$PriPrefix" == "17" ]]
    then
    netmask="255.255.128.0"
    Public_SecPrefix_netmask="255.255.128.0"
    fi
	
 if [[ "$PriPrefix" == "18"  ]]
    then
    netmask="255.255.192.0"
    Public_SecPrefix_netmask="255.255.192.0"
    fi
 
 if [[ "$PriPrefix" == "19" ]]
    then
    netmask="255.255.224.0"
    Public_SecPrefix_netmask="255.255.224.0"
    fi

 if [[ "$PriPrefix" == "20"  ]]
    then
    netmask="255.255.240.0"
    Public_SecPrefix_netmask="255.255.240.0"
    fi

 if [[ "$PriPrefix" == "21"  ]]
    then
    netmask="255.255.248.0"
    Public_SecPrefix_netmask="255.255.248.0"
    fi

 if [[ "$PriPrefix" == "22"  ]]
    then
    netmask="255.255.252.0"
    Public_SecPrefix_netmask="255.255.252.0"
    fi

 if [[ "$PriPrefix" == "23"  ]]
    then
    netmask="255.255.254.0"
    Public_SecPrefix_netmask="255.255.254.0"
    fi

 if [[ "$PriPrefix" == "24" ]]
    then
    netmask="255.255.255.0"
    Public_SecPrefix_netmask="255.255.255.0"
    fi

 if [[ "$PriPrefix" == "25"  ]]
    then
    netmask="255.255.255.128"
    Public_SecPrefix_netmask="255.255.255.128"
    fi

 if [[ "$PriPrefix" == "26"  ]]
    then
    netmask="255.255.255.192"
    Public_SecPrefix_netmask="255.255.255.192"
    fi

 if [[ "$PriPrefix" == "27" ]]
    then
    netmask="255.255.255.224"
    Public_SecPrefix_netmask="255.255.255.224"
    fi

 if [[ "$PriPrefix" == "28" ]]
    then
    netmask="255.255.255.240"
    Public_SecPrefix_netmask="255.255.255.240"
    fi

 if [[ "$PriPrefix" == "29" ]]
    then
    netmask="255.255.255.248"
    Public_SecPrefix_netmask="255.255.255.248"
    fi

 if [[ "$PriPrefix" == "30" ]]
    then
    netmask="255.255.255.252"
    Public_SecPrefix_netmask="255.255.255.252"
    fi

echo "netmask: $netmask" >> $server_name
echo "network: $PriNetwork" >> $server_name

IFS=. read -r m1 m2 m3 m4 <<< "$netmask"

D2B=({0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1})

input=${D2B["$((m1))"]}
invertedInput=$(echo $input | tr 01 10)
invert_netmask1="$((2#$invertedInput))"

input=${D2B["$((m2))"]}
invertedInput=$(echo $input | tr 01 10)
invert_netmask2="$((2#$invertedInput))"


input=${D2B["$((m3))"]}
invertedInput=$(echo $input | tr 01 10)
invert_netmask3="$((2#$invertedInput))"


input=${D2B["$((m4))"]}
invertedInput=$(echo $input | tr 01 10)
invert_netmask4="$((2#$invertedInput))"

inverted_netmask="$invert_netmask1.$invert_netmask2.$invert_netmask3.$invert_netmask4"

IFS=. read -r i1 i2 i3 i4 <<< "$PriNetwork"
IFS=. read -r m1 m2 m3 m4 <<< "$inverted_netmask"

broadcast=""$((i1 |  m1))"."$((i2 | m2))"."$((i3 | m3))"."$((i4 | m4))""

echo "broadcast: $broadcast" >> $server_name
echo "gateway: $PriGateway" >> $server_name
fi

echo "#--------------------------------------------------" >> $server_name
echo "# China_solution" >> $server_name
echo "#---------------------------------------------------" >> $server_name
 if [[ $Public_SecNetwork == $SecNetwork ]]
    then
    echo "Public_SecNetwork: none" >> $server_name
    echo "Public_SecPrefix: none" >> $server_name
    echo "IPADDR_START: none" >> $server_name
    echo "IPADDR_END: none" >> $server_name
    echo "china_solution: false" >> $server_name

    else
    echo "Public_SecNetwork: $Public_SecNetwork" >> $server_name
    echo "Public_SecPrefix: $Public_SecPrefix" >> $server_name

   	Public_SecNetwork_first_octate=$(echo $Public_SecNetwork | cut -f1 -d.)
	Public_SecNetwork_second_octate=$(echo $Public_SecNetwork | cut -f2 -d.)
	Public_SecNetwork_third_octate=$(echo $Public_SecNetwork | cut -f3 -d.)
	Public_SecNetwork_forth_octate=$(echo $Public_SecNetwork | cut -f4 -d.)
	IPADDR_START_octate=$(( Public_SecNetwork_forth_octate + 1 ))
	IPADDR_START="$Public_SecNetwork_first_octate.$Public_SecNetwork_second_octate.$Public_SecNetwork_third_octate.$IPADDR_START_octate"

    echo "IPADDR_START: $IPADDR_START" >> $server_name

 if [[  "$Public_SecPrefix" == "8" ]]
    then
    netmask="255.0.0.0"
    Public_SecPrefix_netmask="255.0.0.0"
    fi
   
 if [[ "$Public_SecPrefix" == "9" ]]
    then
    netmask="255.128.0.0"
    Public_SecPrefix_netmask="255.128.0.0"
    fi
	
 if [[ "$Public_SecPrefix" == "10" ]]
    then
    netmask="255.192.0.0"
    Public_SecPrefix_netmask="255.192.0.0"
    fi
	
 if [[ "$Public_SecPrefix" == "11" ]]
    then
    netmask="255.224.0.0"
    Public_SecPrefix_netmask="255.224.0.0"
    fi
   
 if [[  "$Public_SecPrefix" == "12" ]]
    then
    netmask="255.240.0.0"
    Public_SecPrefix_netmask="255.240.0.0"
    fi
	
 if [[  "$Public_SecPrefix" == "13" ]]
    then
    netmask="255.248.0.0"
    Public_SecPrefix_netmask="255.248.0.0"
    fi
	
 if [[ "$Public_SecPrefix" == "14" ]]
    then
    netmask="255.252.0.0"
    Public_SecPrefix_netmask="255.252.0.0"
    fi
   
 if [[ "$Public_SecPrefix" == "15" ]]
    then
    netmask="255.252.0.0"
    Public_SecPrefix_netmask="255.252.0.0"
    fi
  
 if [[ "$Public_SecPrefix" == "16" ]]
    then
    netmask="255.255.0.0"
    Public_SecPrefix_netmask="255.255.0.0"
    fi
 
 if [[ "$Public_SecPrefix" == "17" ]]
    then
    netmask="255.255.128.0"
    Public_SecPrefix_netmask="255.255.128.0"
    fi
	
 if [[ "$Public_SecPrefix" == "18" ]]
    then
    netmask="255.255.192.0"
    Public_SecPrefix_netmask="255.255.192.0"
    fi
 
 if [[ "$Public_SecPrefix" == "19" ]]
    then
    netmask="255.255.224.0"
    Public_SecPrefix_netmask="255.255.224.0"
    fi

 if [[ "$Public_SecPrefix" == "20" ]]
    then
    netmask="255.255.240.0"
    Public_SecPrefix_netmask="255.255.240.0"
    fi

 if [[ "$Public_SecPrefix" == "21" ]]
    then
    netmask="255.255.248.0"
    Public_SecPrefix_netmask="255.255.248.0"
    fi

 if [[ "$Public_SecPrefix" == "22" ]]
    then
    netmask="255.255.252.0"
    Public_SecPrefix_netmask="255.255.252.0"
    fi

 if [[ "$Public_SecPrefix" == "23" ]]
    then
    netmask="255.255.254.0"
    Public_SecPrefix_netmask="255.255.254.0"
    fi

 if [[ "$Public_SecPrefix" == "24" ]]
    then
    netmask="255.255.255.0"
    Public_SecPrefix_netmask="255.255.255.0"
    fi

 if [[ "$Public_SecPrefix" == "25" ]]
    then
    netmask="255.255.255.128"
    Public_SecPrefix_netmask="255.255.255.128"
    fi

 if [[ "$Public_SecPrefix" == "26" ]]
    then
    netmask="255.255.255.192"
    Public_SecPrefix_netmask="255.255.255.192"
    fi

 if [[ "$Public_SecPrefix" == "27" ]]
    then
    netmask="255.255.255.224"
    Public_SecPrefix_netmask="255.255.255.224"
    fi

 if [[  "$Public_SecPrefix" == "28" ]]
    then
    netmask="255.255.255.240"
    Public_SecPrefix_netmask="255.255.255.240"
    fi

 if [[ "$Public_SecPrefix" == "29" ]]
    then
    netmask="255.255.255.248"
    Public_SecPrefix_netmask="255.255.255.248"
    fi

 if [[ "$Public_SecPrefix" == "30" ]]
    then
    netmask="255.255.255.252"
    Public_SecPrefix_netmask="255.255.255.252"
    fi

	IFS=. read -r m1 m2 m3 m4 <<< "$Public_SecPrefix_netmask"

	D2B=({0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1})

	input=${D2B["$((m1))"]}
	invertedInput=$(echo $input | tr 01 10)
	invert_netmask1="$((2#$invertedInput))"

	input=${D2B["$((m2))"]}
	invertedInput=$(echo $input | tr 01 10)
	invert_netmask2="$((2#$invertedInput))"


	input=${D2B["$((m3))"]}
	invertedInput=$(echo $input | tr 01 10)
	invert_netmask3="$((2#$invertedInput))"


	input=${D2B["$((m4))"]}
	invertedInput=$(echo $input | tr 01 10)
	invert_netmask4="$((2#$invertedInput))"

	inverted_netmask="$invert_netmask1.$invert_netmask2.$invert_netmask3.$invert_netmask4"

IFS=. read -r i1 i2 i3 i4 <<< "$Public_SecNetwork"
IFS=. read -r m1 m2 m3 m4 <<< "$inverted_netmask"

Public_broadcast=""$((i1 |  m1))"."$((i2 | m2))"."$((i3 | m3))"."$((i4 | m4))""

   	Public_broadcast_first_octate=$(echo $Public_broadcast | cut -f1 -d.)
	Public_broadcast_second_octate=$(echo $Public_broadcast | cut -f2 -d.)
	Public_broadcast_third_octate=$(echo $Public_broadcast | cut -f3 -d.)
	Public_broadcast_forth_octate=$(echo $Public_broadcast | cut -f4 -d.)
	IPADDR_END_octate=$(( Public_broadcast_forth_octate - 1 ))
	IPADDR_END="$Public_broadcast_first_octate.$Public_broadcast_second_octate.$Public_broadcast_third_octate.$IPADDR_END_octate"
    
    echo "IPADDR_END: $IPADDR_END" >> $server_name
read -r -p 'Want to configure this server for china (Enter true or false in small) : ' chinasolutione
chinasolutione=${chinasolutione//[[:blank:]]/}
echo "china_solution: $chinasolutione" >> $server_name
    fi


rm -rf Json_sample.txt

echo "hostvar has been created " 
echo " "
cat $server_name 
echo
echo
echo "Note: kindly verify if any changes required kindly do it before going to enter password hostvar is in tools folder"
echo 
echo

read -r -p 'Enter the server Password: ' password
password=${password//[[:blank:]]/}


./sensu.py $server_name $sensu_pass $LuxIP
echo "rabbitmq entry has been created"



mv $server_name ..
cd ..
rm -rf host_vars/$server_name 
mv $server_name host_vars/

read -r -p 'Enter the server SSH port: ' port
port=${port//[[:blank:]]/}


 if [[ $KvmIP == "none" || $KvmIP == "None" ]]
    then
    sed -i -e "/$server_name */d" hosts
    echo "$server_name ansible_host=$LuxIP ansible_port=$port ansble_user=root ansible_ssh_pass='$password'" >> hosts 

    else
    sed -i -e "/$server_name */d" hosts
    echo "$server_name ansible_host=$KvmIP ansible_port=$port ansble_user=root ansible_ssh_pass='$password'" >> hosts
    fi

echo "Host entry has been done"

sudo ansible-playbook -i hosts configure.yml --extra-vars="host=$server_name"






